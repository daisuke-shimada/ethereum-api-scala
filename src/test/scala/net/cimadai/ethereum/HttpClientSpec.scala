package net.cimadai.ethereum

import net.cimadai.utils.HexBytesUtil
import org.scalatest.FunSpec

class HttpClientSpec extends FunSpec {
    //private val host = "makanai:8545"
    private val host = "localhost:8545"
    private val port = 8545

    private def assertExists(name: String, result: Either[Option[JsonErrors], JsonRPCResponse]): Unit = {
        assert(result.isRight, true)
        println(name + " - " + result.right.get)
    }

    describe("dispatch getting-started spec") {
        it("api can work") {
            if (host.nonEmpty) {
                val cli = new EthereumRPCApiClient(host, port)

                val web3 = cli.Web3
                val net = cli.Net
                val eth = cli.Eth

                assertExists("web3.clientVersion", web3.clientVersion())

                val res_web3_sha3 = web3.sha3(Array[Byte](0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64))
                assertExists("web3.sha3", res_web3_sha3)
                assert(res_web3_sha3.right.get.result == "0x47173285a8d7341e5e972fc677286384f802f8ef42a5ec5f03bbfa254cb01fad", true)


                assertExists("net.version", net.version())

                assertExists("net.listening", net.listening())

                assertExists("net.peerCount", net.peerCount())


                assertExists("eth.protocolVersion", eth.protocolVersion())

                assertExists("eth.syncing", eth.syncing())

                assertExists("eth.coinbase", eth.coinbase())

                assertExists("eth.mining", eth.mining())

                assertExists("eth.hashrate", eth.hashrate())

                assertExists("eth.gasPrice", eth.gasPrice())

                val res_eth_accounts = eth.accounts()
                assertExists("eth.res_eth_accounts", res_eth_accounts)
                val account = res_eth_accounts.right.get.result.asInstanceOf[List[String]].head

                val res_eth_blockNumber = eth.blockNumber()
                assertExists("eth.blockNumber", eth.blockNumber())
                val blockNumber = res_eth_blockNumber.right.get.result.asInstanceOf[String]

                val wei = HexBytesUtil.hex2bigdecimal(eth.getBalance(account, "latest").right.get.result.asInstanceOf[String])
                val kwei = wei / 1000
                val mwei = kwei / 1000
                val gwei = mwei / 1000
                val szabo = gwei / 1000
                val finney = szabo / 1000
                val ether = finney / 1000
                println(s"$account's wei is")
                println(s"\twei = $wei")
                println(s"\tkwei = $kwei")
                println(s"\tmwei = $mwei")
                println(s"\tgwei = $gwei")
                println(s"\tszabo = $szabo")
                println(s"\tfinney = $finney")
                println(s"\tether = $ether")

                assertExists("eth.getBalance(blockNumber)", eth.getBalance(account, blockNumber))
                assertExists("eth.getBalance(latest)", eth.getBalance(account, "latest"))
                assertExists("eth.getBalance(earliest)", eth.getBalance(account, "earliest"))
                assertExists("eth.getBalance(pending)", eth.getBalance(account, "pending"))

                val account2 = "0x67b6d4f4b02ee508ffe14bd54a4b842501ab81f3"
                val wei2 = HexBytesUtil.hex2bigdecimal(eth.getBalance(account2, "latest").right.get.result.asInstanceOf[String])
                val eth2 = wei2 / 1000 / 1000 / 1000 / 1000 / 1000 / 1000
                println(s"$account2's wei is")
                println(s"\twei = $wei2")
                println(s"\tether = $eth2")

                val node3 = "0x71be990c935c658961626a75280d0aca7a8df95f"
                val node2 = "0x67b6d4f4b02ee508ffe14bd54a4b842501ab81f3"
                val sendEth = EthBalance.fromUnit(1000.0, EthUnit.ether)
                assertExists("eth.sendTransaction", eth.sendTransaction(node3, Some(node2), None, Some(sendEth.toWeiHex), ""))

                val account3 = "0x67b6d4f4b02ee508ffe14bd54a4b842501ab81f3"
                val wei3 = HexBytesUtil.hex2bigdecimal(eth.getBalance(account3, "latest").right.get.result.asInstanceOf[String])
                val eth3 = wei3 / 1000 / 1000 / 1000 / 1000 / 1000 / 1000
                println(s"$account2's wei is")
                println(s"\twei = $wei3")
                println(s"\tether = $eth3")

                assertExists("eth.getStorageAt(account, 0x0, blockNumber)", eth.getStorageAt(account, "0x0", blockNumber))
                assertExists("eth.getStorageAt(account, 0x0, latest)", eth.getStorageAt(account, "0x0", "latest"))
                assertExists("eth.getStorageAt(account, 0x0, earliest)", eth.getStorageAt(account, "0x0", "earliest"))
                assertExists("eth.getStorageAt(account, 0x0, pending)", eth.getStorageAt(account, "0x0", "pending"))

                assertExists("eth.getTransactionCount(account, blockNumber)", eth.getTransactionCount(account, blockNumber))
                assertExists("eth.getTransactionCount(account, latest)", eth.getTransactionCount(account, "latest"))
                assertExists("eth.getTransactionCount(account, earliest)", eth.getTransactionCount(account, "earliest"))
                assertExists("eth.getTransactionCount(account, pending)", eth.getTransactionCount(account, "pending"))


                assertExists("eth.getBlockByNumber", eth.getBlockByNumber("0x4F22", isFullTransaction = true))
                assertExists("eth.getBlockByNumber", eth.getBlockByNumber("latest", isFullTransaction = true))
                assertExists("eth.getBlockByNumber", eth.getBlockByNumber("earliest", isFullTransaction = true))
                assertExists("eth.getBlockByNumber", eth.getBlockByNumber("pending", isFullTransaction = true))
                val latestBlockInfoJson = eth.getBlockByNumber("latest", isFullTransaction = true).right.get
                val blockInfo = eth.getAsBlockInfo(latestBlockInfoJson.result.asInstanceOf[Map[String, Any]])

                assertExists("eth.getBlockTransactionCountByHash(blockInfo.hash)", eth.getBlockTransactionCountByHash(blockInfo.hash))

                assertExists("eth.getCompilers", eth.getCompilers)

                assertExists("eth.compileSolidity", eth.compileSolidity("contract test { function multiply(uint a) returns(uint d) { return a * 7; } }"))


                val source = "contract TestContract {  function multiple(int a, int b) constant returns(int) { return a * b; } }"
                val compileResult = eth.compileSolidity(source)
                assertExists("eth.compileSolidity", compileResult)
                val map = compileResult.right.get.result.asInstanceOf[Map[String, Any]]
                val compiledContract = eth.getAsCompiledContract(map.head._2.asInstanceOf[Map[String, Any]])

                val callData = compiledContract.info.abiDefinition.find(_.name == "multiple").map(abi => {
                    s"${abi.name}(${abi.inputs.map(_.`type`).mkString(",")})"
                }).map(plain => {
                    val hash = web3.sha3(plain.getBytes()).right.get.result.asInstanceOf[String]
                    hash.substring(0, 10) + HexBytesUtil.int2hex(3, 64) + HexBytesUtil.int2hex(2, 64)
                })


                val cont1 = "0x8b67b090cc89bea341c2308c4f9b1221aefcca02"

                // Contractの登録
                val contractCommitRes = eth.sendTransaction(node3, None, None, None, compiledContract.code)
                assertExists("eth.sendTransaction", contractCommitRes)
                val contractCommitTransactionHash = contractCommitRes.right.get.result.asInstanceOf[String]

                //        val filterRes = eth.newFilter(address = Some(contractCommitTransactionHash))
                //        assertExists("eth.newFilter", filterRes)
                //        val filterId = filterRes.right.get.result.asInstanceOf[String]

                var contractStatus = eth.getAsContractTransactionInfo(eth.getTransactionReceipt(contractCommitTransactionHash).right.get.result.asInstanceOf[Map[String, Any]])
                while (contractStatus.isEmpty || contractStatus.get.contractAddress.isEmpty) {
                    println("Wait for contract is mined.")
                    Thread.sleep(1000)
                    contractStatus = eth.getAsContractTransactionInfo(eth.getTransactionReceipt(contractCommitTransactionHash).right.get.result.asInstanceOf[Map[String, Any]])
                }

                println(contractStatus.get)

                //        val data = "0x6ffa1caa0000000000000000000000000000000000000000000000000000000000000005"
                assertExists("eth.call", eth.call(node3, cont1, None, callData.get))
            }
        }
    }
}
