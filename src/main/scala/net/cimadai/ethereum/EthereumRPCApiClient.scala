package net.cimadai.ethereum

import dispatch._
import net.cimadai.utils.HexBytesUtil
import org.json4s.JsonAST._
import org.json4s.JsonDSL._

import scala.collection.mutable

// cf: https://github.com/ethereum/wiki/wiki/JavaScript-API


class EthereumRPCApiClient(host: String, port: Int) extends HttpClient {
  private val baseUrl = s"http://$host:$port/"

  override def decorateRequest(req: Req): Req = {
    req
  }

  private def uri(uri: String) = baseUrl + uri

  private val id = 67

  private def mappingToJsonValue(param: Any): JValue = {
    param match {
      case p: String => JString(p)
      case p: Int => JInt(p)
      case p: Boolean => JBool(p)
      case p: List[Any] => JArray(p.map(mappingToJsonValue))
      case p: Map[_, _] => JObject(p.map(v => JField(v._1.toString, mappingToJsonValue(v._2))).toList)
      case _: Any => null
    }
  }

  private def createRPCData(method: String, params: List[Any] = List.empty): String = {
    val paramsAsJson = mappingToJsonValue(params)
    compact(render(("jsonrpc" -> "2.0") ~ ("method" -> method) ~ ("params" -> paramsAsJson) ~ ("id" -> id)))
  }

  object Web3 {
    /**
      * Returns the current client version.
      *
      * @return String: The current client version
      */
    def clientVersion(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("web3_clientVersion")))
    }

    /**
      * Returns Keccak-256 (not the standardized SHA3-256) of the given data.
      *
      * @param bytes the data to convert into a SHA3 hash.
      * @return DATA - The SHA3 result of the given string.
      */
    def sha3(bytes: Array[Byte]): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("web3_sha3", List(HexBytesUtil.bytes2hex(bytes)))))
    }
  }

  object Net {
    /**
      * Returns the current network protocol version.
      *
      * @return String - The current network protocol version
      */
    def version(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("net_version")))
    }

    /**
      * Returns true if client is actively listening for network connections.
      *
      * @return Boolean - true when listening, otherwise false.
      */
    def listening(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("net_listening")))
    }

    /**
      * Returns number of peers currenly connected to the client.
      *
      * @return QUANTITY - integer of the number of connected peers.
      */
    def peerCount(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("net_peerCount")))
    }
  }

  object Eth {
    /**
      * Returns the current ethereum protocol version.
      *
      * @return String - The current ethereum protocol version
      */
    def protocolVersion(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_protocolVersion")))
    }

    /**
      * Returns an object with data about the sync status or false.
      *
      * @return Object|Boolean, An object with sync status data or FALSE, when not syncing:
      *             startingBlock: QUANTITY - The block at which the import started (will only be reset, after the sync reached his head)
      *             currentBlock: QUANTITY - The current block, same as eth_blockNumber
      *             highestBlock: QUANTITY - The estimated highest block
      */
    def syncing(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_syncing")))
    }

    /**
      * Returns the client coinbase address.
      *
      * @return DATA, 20 bytes - the current coinbase address.
      */
    def coinbase(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_coinbase")))
    }

    /**
      * Returns true if client is actively mining new blocks.
      *
      * @return Boolean - returns true of the client is mining, otherwise false.
      */
    def mining(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_mining")))
    }

    /**
      * Returns the number of hashes per second that the node is mining with.
      *
      * @return QUANTITY - number of hashes per second.
      */
    def hashrate(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_hashrate")))
    }

    /**
      * Returns the current price per gas in wei.
      *
      * @return QUANTITY - integer of the current gas price in wei.
      */
    def gasPrice(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_gasPrice")))
    }

    /**
      * Returns a list of addresses owned by client.
      *
      * @return Array of DATA, 20 Bytes - addresses owned by the client.
      */
    def accounts(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_accounts")))
    }

    /**
      * Returns the number of most recent block.
      *
      * @return QUANTITY - integer of the current block number the client is on.
      */
    def blockNumber(): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_blockNumber")))
    }

    /**
      * Returns the balance of the account of given address.
      *
      * @param address DATA, 20 Bytes - address to check for balance.
      * @param block QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending", see the default block parameter
      *
      * @return QUANTITY - integer of the current balance in wei.
      */
    def getBalance(address: String, block: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getBalance", List(address, block))))
    }

    /**
      * Returns the value from a storage position at a given address.
      *
      * @param address DATA, 20 Bytes - address of the storage.
      * @param position QUANTITY - integer of the position in the storage.
      * @param block QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending", see the default block parameter
      *
      * @return DATA - the value at this storage position.
      */
    def getStorageAt(address: String, position: String, block: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getStorageAt", List(address, position, block))))
    }

    /**
      * Returns the number of transactions sent from an address.
      *
      * @param address DATA, 20 Bytes - address.
      * @param block QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending", see the default block parameter
      *
      * @return QUANTITY - integer of the number of transactions send from this address.
      */
    def getTransactionCount(address: String, block: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getTransactionCount", List(address, block))))
    }

    /**
      * Returns the number of transactions in a block from a block matching the given block hash.
      *
      * @param blockHash DATA, 32 Bytes - hash of a block
      *
      * @return QUANTITY - integer of the number of transactions in this block.
      */
    def getBlockTransactionCountByHash(blockHash: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getBlockTransactionCountByHash", List(blockHash))))
    }

    /**
      * Returns the number of transactions in a block from a block matching the given block number.
      *
      * @param blockHash QUANTITY|TAG - integer of a block number, or the string "earliest", "latest" or "pending", as in the default block parameter.
      *
      * @return QUANTITY - integer of the number of transactions in this block.
      */
    def getBlockTransactionCountByNumber(blockHash: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getBlockTransactionCountByNumber", List(blockHash))))
    }
    //    eth_getBlockTransactionCountByHash
    //    eth_getBlockTransactionCountByNumber
    //    eth_getUncleCountByBlockHash
    //    eth_getUncleCountByBlockNumber
    //    eth_getCode
    //    eth_sign

    def sendTransaction(from: String, to: Option[String], gas: Option[Int], value: Option[String], data: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      val options = mutable.Map[String, Any]("from" -> from, "data" -> data)
      to.foreach(t => options.put("to", t))
      gas.foreach(g => options.put("gas", g))
      value.foreach(v => options.put("value", v))
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_sendTransaction", List(options.toMap))))
    }

    //    eth_sendRawTransaction

    //    eth_call
    def call(from: String, to: String, gas: Option[Int], data: String, block: String = "latest"): Either[Option[JsonErrors], JsonRPCResponse] = {
      val options = mutable.Map[String, Any]("from" -> from, "to" -> to, "data" -> data)
      gas.foreach(g => options.put("gas", g))
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_call", List(options.toMap, block))))
    }

    //    eth_estimateGas
    def estimateGas(from: String, to: String, gas: Option[Int], data: String, block: String = "latest"): Either[Option[JsonErrors], JsonRPCResponse] = {
      val options = mutable.Map[String, Any]("from" -> from, "to" -> to, "data" -> data)
      gas.foreach(g => options.put("gas", g))
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_estimateGas", List(options.toMap, block))))
    }
    //    eth_getBlockByHash
    //    eth_getBlockByNumber

    /**
      * Returns information about a block by block number.
      *
      * @param block QUANTITY|TAG - integer of a block number, or the string "earliest", "latest" or "pending", as in the default block parameter.
      * @param isFullTransaction Boolean - If true it returns the full transaction objects, if false only the hashes of the transactions.
      * @return Object - A block object, or null when no block was found:
      *              number: QUANTITY - the block number. null when its pending block.
      *              hash: DATA, 32 Bytes - hash of the block. null when its pending block.
      *              parentHash: DATA, 32 Bytes - hash of the parent block.
      *              nonce: DATA, 8 Bytes - hash of the generated proof-of-work. null when its pending block.
      *              sha3Uncles: DATA, 32 Bytes - SHA3 of the uncles data in the block.
      *              logsBloom: DATA, 256 Bytes - the bloom filter for the logs of the block. null when its pending block.
      *              transactionsRoot: DATA, 32 Bytes - the root of the transaction trie of the block.
      *              stateRoot: DATA, 32 Bytes - the root of the final state trie of the block.
      *              receiptsRoot: DATA, 32 Bytes - the root of the receipts trie of the block.
      *              miner: DATA, 20 Bytes - the address of the beneficiary to whom the mining rewards were given.
      *              difficulty: QUANTITY - integer of the difficulty for this block.
      *              totalDifficulty: QUANTITY - integer of the total difficulty of the chain until this block.
      *              extraData: DATA - the "extra data" field of this block.
      *              size: QUANTITY - integer the size of this block in bytes.
      *              gasLimit: QUANTITY - the maximum gas allowed in this block.
      *              gasUsed: QUANTITY - the total used gas by all transactions in this block.
      *              timestamp: QUANTITY - the unix timestamp for when the block was collated.
      *              transactions: Array - Array of transaction objects, or 32 Bytes transaction hashes depending on the last given parameter.
      *              uncles: Array - Array of uncle hashes.
      */
    def getBlockByNumber(block: String, isFullTransaction: Boolean): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getBlockByNumber", List(block, isFullTransaction))))
    }

    def getAsBlockInfo(blockObject: Map[String, Any]): BlockInfo = {
      BlockInfoJSON.getClass.getMethods.find(_.getName == "apply")
        .get.invoke(BlockInfoJSON, blockObject.values.toList.map(_.asInstanceOf[AnyRef]):_*)
        .asInstanceOf[BlockInfoJSON].toBlockInfo
    }

    private def getOrElse[T](map: Map[String, Any], key: String, defaultValue: T): T = {
      map.getOrElse(key, defaultValue).asInstanceOf[T]
    }
    private def getOrNone[T](map: Map[String, Any], key: String): Option[T] = {
      map.get(key).map(_.asInstanceOf[T])
    }

    def getAsCompiledContract(compiledContractObject: Map[String, Any]): CompiledContract = {
      val code = getOrElse(compiledContractObject, "code", "")
      val info = getOrElse(compiledContractObject, "info", Map[String, Any]())

      val source = getOrElse(info, "source", "")
      val abiDefinition = getOrElse(info, "abiDefinition", List[Map[String, Any]]())
        .map(abiDef => {
          val defType = getOrElse(abiDef, "type", "")
          val inputs = getOrElse(abiDef, "inputs", List[Map[String, Any]]()).map(input => {
            val name = getOrElse(input, "name", "")
            val valueType = getOrElse(input, "type", "")
            val indexed = getOrNone[Boolean](input, "indexed")
            ContractAbiIO(name, valueType, indexed)
          })
          val outputs = getOrElse(abiDef, "outputs", List[Map[String, Any]]()).map(input => {
            val name = getOrElse(input, "name", "")
            val valueType = getOrElse(input, "type", "")
            ContractAbiIO(name, valueType, None)
          })
          val name = getOrElse(abiDef, "name", "")
          val constant = getOrNone[Boolean](abiDef, "constant")
          val anonymous = getOrNone[Boolean](abiDef, "anonymous")
          ContractAbi(constant, anonymous, inputs, name, outputs, defType)
        })
      val compilerOptions = getOrElse(info, "compilerOptions", "")
      val language = getOrElse(info, "language", "")
      val userDoc = getOrElse(info, "userDoc", Map[String, Any]())
      val developerDoc = getOrElse(info, "developerDoc", Map[String, Any]())
      val compilerVersion = getOrElse(info, "compilerVersion", "")
      val languageVersion = getOrElse(info, "languageVersion", "")

      val infoJson = CompiledContractInfo(source, abiDefinition, compilerOptions, language, userDoc, developerDoc, compilerVersion, languageVersion)
      CompiledContract(code, infoJson)
    }

    def getAsContractTransactionInfo(mapOrNull: Map[String, Any]): Option[ContractTransactionInfo] = {
      Option(mapOrNull).map(map => {
        val cumulativeGasUsed = getOrElse(map, "cumulativeGasUsed", "")
        val blockHash = getOrElse(map, "blockHash", "")
        val root = getOrElse(map, "root", "")
        val contractAddress = getOrElse(map, "contractAddress", "")
        val from = getOrElse(map, "from", "")
        val to = getOrElse(map, "to", "")
        val blockNumber = getOrElse(map, "blockNumber", "")
        val gasUsed = getOrElse(map, "gasUsed", "")
        val transactionIndex = getOrElse(map, "transactionIndex", "")
        val transactionHash = getOrElse(map, "transactionHash", "")
        val logs = getOrElse(map, "logs", List[Any]())

        ContractTransactionInfo(cumulativeGasUsed, blockHash, root, contractAddress, from, to, blockNumber, gasUsed, transactionIndex, transactionHash, logs)
      })
    }

    //    eth_getTransactionByHash
    //    eth_getTransactionByBlockHashAndIndex
    //    eth_getTransactionByBlockNumberAndIndex

    //    eth_getTransactionReceipt
    def getTransactionReceipt(transactionHash: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getTransactionReceipt", List(transactionHash))))
    }

    //    eth_getUncleByBlockHashAndIndex
    //    eth_getUncleByBlockNumberAndIndex

    /**
      * Returns a list of available compilers in the client.
      *
      * @return Array - Array of available compilers.
      */
    def getCompilers: Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getCompilers", List())))
    }


    /**
      * Returns compiled solidity code.
      *
      * @param source String - The source code.
      *
      * @return DATA - The compiled source code.
      */
    def compileSolidity(source: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_compileSolidity", List(source))))
    }

    //    eth_compileLLL
    //    eth_compileSerpent

    /**
      * Creates a filter object, based on filter options, to notify when the state changes (logs).
      * To check if the state has changed, call eth_getFilterChanges.
      *
      * @param fromBlock: QUANTITY|TAG - (optional, default: "latest") Integer block number, or "latest" for
      *                 the last mined block or "pending", "earliest" for not yet mined transactions.
      * @param toBlock: QUANTITY|TAG - (optional, default: "latest") Integer block number, or "latest" for
      *               the last mined block or "pending", "earliest" for not yet mined transactions.
      * @param address: DATA|Array, 20 Bytes - (optional) Contract address or a list of addresses from which logs should originate.
      * @param topics: Array of DATA, - (optional) Array of 32 Bytes DATA topics. Topics are order-dependent.
      *              Each topic can also be an array of DATA with "or" options.
      *
      *              A note on specifying topic filters:
      *              Topics are order-dependent. A transaction with a log with topics [A, B] will be matched by the following topic filters:
      *
      *              [] "anything"
      *              [A] "A in first position (and anything after)"
      *              [null, B] "anything in first position AND B in second position (and anything after)"
      *              [A, B] "A in first position AND B in second position (and anything after)"
      *              [ [A, B], [A, B] ] "(A OR B) in first position AND (A OR B) in second position (and anything after)"
      *
      * @return
      */
    def newFilter(fromBlock: String = "latest", toBlock: String = "latest", address: Option[String] = None, topics: List[String] = List.empty): Either[Option[JsonErrors], JsonRPCResponse] = {
      val filter = mutable.Map[String, Any]("fromBlock" -> fromBlock, "toBlock" -> toBlock)
      address.foreach(a => filter.put("address", a))
      if (topics.nonEmpty) { filter.put("topics", topics) }

      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_newFilter", List(filter.toMap))))
    }
    //    eth_newBlockFilter
    //    eth_newPendingTransactionFilter
    //    eth_uninstallFilter

    //    eth_getFilterChanges
    def getFilterChanges(filterId: String): Either[Option[JsonErrors], JsonRPCResponse] = {
      postAs[JsonErrors, JsonRPCResponse](uri(""), Some(createRPCData("eth_getFilterChanges", List(filterId))))
    }

    //    eth_getFilterLogs
    //    eth_getLogs
    //    eth_getWork
    //    eth_submitWork
    //    eth_submitHashrate

  }

}

