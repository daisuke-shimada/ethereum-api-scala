package net.cimadai.ethereum

import net.cimadai.utils.{HexBytesUtil, EthUtil}
import net.cimadai.utils.HexBytesUtil.{hex2int => h2i}
import net.cimadai.utils.HexBytesUtil.{hex2long => h2l}

case class EthAddress(hex: String)

case class EthBalance(wei: BigDecimal) {
  def toWeiHex: String = {
    "0x" + wei.toBigInt().toString(16)
  }
}

object EthBalance {
  def fromHex(hex: String): EthBalance = {
    EthBalance(HexBytesUtil.hex2bigdecimal(hex))
  }

  def fromUnit(value: Double, unit: EthUnit.Value): EthBalance = {
    EthBalance(EthUtil.toWei(value, unit))
  }
}

object EthUnit extends Enumeration {
  val Wei = Value(0)
  val Kwei = Value(1)
  val Mwei = Value(2)
  val Gwei = Value(3)
  val szabo = Value(4)
  val finney = Value(5)
  val ether = Value(6)
  val Kether = Value(7)
  val Mether = Value(8)
  val Gether = Value(9)
  val Tether = Value(10)
}

case class JsonRPCResponse(id: Int, jsonrpc: String, result: Any)

case class BlockInfo(number: Int, difficulty: Int, timestamp: Int, size: Int
  , nonce: String, sha3Uncles: String, stateRoot: String, totalDifficulty: Long, logsBloom: String
  , parentHash: String, uncles: List[String], miner: String, hash: String, transactionsRoot: String
  , extraData: String, gasLimit: Int, gasUsed: Int, receiptRoot: String, transactions: List[String])

case class BlockInfoJSON(number: String, difficulty: String, timestamp: String, size: String
  , nonce: String, sha3Uncles: String, stateRoot: String, totalDifficulty: String, logsBloom: String
  , parentHash: String, uncles: List[String], miner: String, hash: String, transactionsRoot: String
  , extraData: String, gasLimit: String, gasUsed: String, receiptRoot: String, transactions: List[String]) {

  def toBlockInfo: BlockInfo = {
    BlockInfo(h2i(number), h2i(difficulty), h2i(timestamp), h2i(size), nonce, sha3Uncles, stateRoot
      , h2l(totalDifficulty), logsBloom, parentHash, uncles, miner, hash, transactionsRoot, extraData
      , h2i(gasLimit), h2i(gasUsed), receiptRoot, transactions)
  }
}

case class CompiledContract(code: String, info: CompiledContractInfo)
case class CompiledContractInfo(source: String, abiDefinition: List[ContractAbi], compilerOptions: String
  , language: String, userDoc: Map[String, Any], developerDoc: Map[String, Any], compilerVersion: String, languageVersion: String)
case class ContractAbi(constant: Option[Boolean], anonymous: Option[Boolean], inputs: List[ContractAbiIO], name: String, outputs: List[ContractAbiIO], `type`: String)
case class ContractAbiIO(name: String, `type`: String, indexed: Option[Boolean])

case class ContractTransactionInfo(cumulativeGasUsed: String, blockHash: String, root: String, contractAddress: String
  , from: String, to: String, blockNumber: String, gasUsed: String, transactionIndex: String, transactionHash: String, logs: List[Any])

case class JsonErrors(errors: List[String])
