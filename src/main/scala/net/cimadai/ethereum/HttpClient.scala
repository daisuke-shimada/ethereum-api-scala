package net.cimadai.ethereum

import com.ning.http.client.Response
import dispatch.Defaults._
import dispatch._
import org.json4s._
import org.json4s.jackson.JsonMethods

import scala.concurrent.duration._
import scala.concurrent.{Await, TimeoutException}

trait HttpClient extends JsonMethods {

  object HTTP_METHOD extends Enumeration {
    val GET, POST, PUT, DELETE = Value
  }

  private val absoluteTimeout = 30.seconds
  protected val StatusCodeUndefined = -1
  implicit val formats = DefaultFormats

  private val http = new Http

  /**
   * A method for appending application specified request headers if it required.
   */
  protected def decorateRequest(req: Req): Req = req

  val CONTENT_TYPE_FORM = Map("Content-Type" -> "application/x-www-form-urlencoded")
  val CONTENT_TYPE_JSON = Map("Content-Type" -> "application/json")
  def get(uri: String) = executeRequestAndAwait(uri, HTTP_METHOD.GET, None, Some(Map.empty), Some(Map.empty))
  def post(uri: String, body: Option[String], params: Option[HttpParams], headers: Option[HttpHeaders]) = executeRequestAndAwait(uri, HTTP_METHOD.POST, body, params, headers)
  def put(uri: String, params: HttpParams) = executeRequestAndAwait(uri, HTTP_METHOD.PUT, None, Some(params), Some(CONTENT_TYPE_FORM))
  def delete(uri: String, params: HttpParams) = executeRequestAndAwait(uri, HTTP_METHOD.DELETE, None, Some(params), Some(CONTENT_TYPE_FORM))

  protected def parseAs[ERR, T](response: Response)
      (implicit mft: scala.reflect.Manifest[T], mfe: scala.reflect.Manifest[ERR]): Either[Option[ERR], T] = {
    try {
      val camelized = parse(response.getResponseBody).camelizeKeys
      camelized.extractOpt[T] match {
        case Some(resp) => Right(resp)
        case _ => Left(camelized.extractOpt[ERR])
      }
    } catch {
      case e: Throwable => Left(None)
    }
  }
  protected def getAs[ERR, T](url: String, paramsOrNone: Option[HttpParams] = None)
      (implicit mft: scala.reflect.Manifest[T], mfe: scala.reflect.Manifest[ERR]): Either[Option[ERR], T] = {
    get(url + paramsOrNone.map(_.toQueryString).getOrElse("")) match {
      case Some(response) => parseAs[ERR, T](response)
      case _ => Left(None)
    }
  }
//  protected def postAs[ERR, T](url: String, params: HttpParams = Map.empty)
//      (implicit mft: scala.reflect.Manifest[T], mfe: scala.reflect.Manifest[ERR]): Either[Option[ERR], T] = {
//    postAs(url, None, Some(params), Some(CONTENT_TYPE_FORM))
//  }
  protected def postAs[ERR, T](url: String, body: Option[String] = None, params: Option[HttpParams] = None, headers: Option[HttpHeaders] = None)
    (implicit mft: scala.reflect.Manifest[T], mfe: scala.reflect.Manifest[ERR]): Either[Option[ERR], T] = {
    post(url, body, params, headers) match {
      case Some(response) => parseAs[ERR, T](response)
      case _ => Left(None)
    }
  }
  protected def putAs[ERR, T](url: String, params: HttpParams = Map.empty)
      (implicit mft: scala.reflect.Manifest[T], mfe: scala.reflect.Manifest[ERR]): Either[Option[ERR], T] = {
    put(url, params) match {
      case Some(response) => parseAs[ERR, T](response)
      case _ => Left(None)
    }
  }

  type HttpParams = Map[String, Seq[String]]
  type HttpHeaders = Map[String, String]
  implicit class ParamsExtension(params: HttpParams) {
    def toQueryString: String = {
      "?" + params.flatMap(param => { s"${param._1}=${param._2.head}" }).mkString("&")
    }
  }

  /**
   * Execute a request and await for the response.
   * @param uri A request URL
   * @param method HTTP_METHOD (GET/POST/PUT/DELETE)
   * @param params Request parameters
   * @return Response or None
   */
  private def executeRequestAndAwait(uri: String, method: HTTP_METHOD.Value, body: Option[String], params: Option[HttpParams], headers: Option[HttpHeaders] = None): Option[Response] = {
    try {

      val req = body match {
        case Some(b) => url(uri).setMethod(method.toString).setBody(b)
        case None => url(uri).setMethod(method.toString)
      }

      params match {
        case Some(p) => req.setParameters(p)
        case None =>
      }

      headers match {
        case Some(h) =>
          h.foreach { case (key, value) => req.addHeader(key, value) }
        case None =>
          if (method != HTTP_METHOD.GET) {
            req.addHeader("Content-Type", "application/x-www-form-urlencoded")
          }
      }

      Option(Await.result(http(decorateRequest(req)), absoluteTimeout))
    } catch {
      case e: TimeoutException =>
        None
    }
  }
}
