package net.cimadai.utils

import net.cimadai.ethereum.{EthAddress, EthUnit}
import scala.util.Random

object EthUtil {
  val md = java.security.MessageDigest.getInstance("SHA-1")

  def toWei(value: Double, unit: EthUnit.Value): BigDecimal = {
    BigDecimal(value) * BigDecimal(1000).pow(unit.id)
  }

  def generateNewAddress(): EthAddress = {
    EthAddress(md.digest((new Random).nextString(20).getBytes("UTF-8")).map("%02x".format(_)).mkString)
  }

  def substrAsAddress(str: String): String = {
    if (str.length == 64) {
      str.substring(24, 64)
    } else {
      ""
    }
  }

}
