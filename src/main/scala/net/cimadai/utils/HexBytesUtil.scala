package net.cimadai.utils

import scala.language.implicitConversions

object HexBytesUtil {

  def hex2bigdecimal(hex: String): BigDecimal = BigDecimal(BigInt(hex.replaceFirst("0x", ""), 16))

  def hex2int(hex: String): Int = Integer.parseInt(hex.replaceFirst("0x", ""), 16)

  def hex2long(hex: String): Long = java.lang.Long.valueOf(hex.replaceFirst("0x", ""), 16)

  def hex2bytes(hex: String): Array[Byte] = {
    if (hex.contains(" ")) {
      hex.split(" ").map(Integer.parseInt(_, 16).toByte)
    } else if (hex.contains("-")) {
      hex.split("-").map(Integer.parseInt(_, 16).toByte)
    } else {
      hex.sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte)
    }
  }

  def bytes2hex(bytes: Array[Byte], sep: Option[String] = None): String = {
    sep match {
      case None => bytes.map("%02x".format(_)).mkString
      case _ => bytes.map("%02x".format(_)).mkString(sep.get)
    }
  }

  def zeroPadding(str: String, digit: Int): String = {
    if (digit > 0) {
      (("0" * digit) + str).takeRight(digit)
    } else {
      str
    }
  }

  def int2hex(value: Int, digit: Int = 0): String = {
    zeroPadding(Integer.toHexString(value), digit)
  }

  def long2hex(value: Long, digit: Int = 0): String = {
    zeroPadding(value.toHexString, digit)
  }

  def bigdecimal2hex(value: BigDecimal, digit: Int = 0): String = {
    zeroPadding(value.toBigInt().toString(16), digit)
  }

  implicit private def toHexString(l: Long): String = {
    val zeros = "00000000" // 8 zeros
    @inline def padBinary8(i: Int) = {
      val s = Integer.toHexString(i)
      zeros.substring(s.length) + s
    }

    val lo = l.toInt
    val hi = (l >>> 32).toInt

    if (hi != 0) Integer.toHexString(hi) + padBinary8(lo)
    else Integer.toHexString(lo)
  }
}
